﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Microsoft.Extensions.Configuration.Legacy
{
    internal static class ConfigurationExtensions
    {
        //public const char KeyDelimiter = "."
        internal const string AppSettings = "appSettings";

        internal static string GetAppSetting(this IConfiguration configuration, string name)
        {
            return configuration?.GetSection(AppSettings)[name];
        }

        /// <summary>
        ///     Gets all of the configuration sections for a set of keys
        /// </summary>
        /// <param name="sectionNames">The names of the sections from the top-most level to lowest</param>
        internal static ReadOnlyDictionary<string, IConfigurationSection> GetSection(this IConfiguration configuration,
            params string[] sectionNames)
        {
            if (sectionNames.Length == 0)
                return new ReadOnlyDictionary<string, IConfigurationSection>(
                    new Dictionary<string, IConfigurationSection>());

            var fullKey = string.Join(ConfigurationPath.KeyDelimiter, sectionNames);

            return new ReadOnlyDictionary<string, IConfigurationSection>(configuration?.GetSection(fullKey)
                .GetChildren()
                ?.ToDictionary(x => x.Key, x => x));
        }

        /// <summary>
        ///     Given a set of keys, will join them and get the value at that level
        /// </summary>
        /// <param name="keys">Names of the sections from top-level to lowest level</param>
        /// <returns>The value of that key</returns>
        internal static string GetValue(this IConfiguration configuration, params string[] keys)
        {
            if (keys.Length == 0)
                throw new ArgumentException("Need to provide keys", nameof(keys));

            var fullKey = string.Join(ConfigurationPath.KeyDelimiter, keys);

            return configuration?[fullKey];
        }
    }
}