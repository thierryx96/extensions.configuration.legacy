
namespace Microsoft.Extensions.Configuration.Legacy
{
    internal enum ConfigurationAction
    {
        Add,
        Remove,
        Clear
    }
}