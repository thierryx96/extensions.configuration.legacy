﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Microsoft.Extensions.Configuration.Legacy
{
    internal class LegacyConfigurationSource : IConfigurationSource
    {
        public LegacyConfigurationSource(string configuration, bool loadFromFile, bool optional,
            params IConfigurationParser[] parsers)
        {
            LoadFromFile = loadFromFile;
            Configuration = configuration;
            Optional = optional;

            var parsersToUse = new List<IConfigurationParser>
            {
                new KeyValueParser(),
                new KeyValueParser("name", "connectionString")
            };

            parsersToUse.AddRange(parsers);

            Parsers = parsersToUse.ToArray();
        }

        public string Configuration { get; set; }
        public bool LoadFromFile { get; set; }
        public bool Optional { get; set; }
        public IEnumerable<IConfigurationParser> Parsers { get; set; }

        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new LegacyConfigurationProvider(Configuration, LoadFromFile, Optional, Parsers);
        }
    }
}